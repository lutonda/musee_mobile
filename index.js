import { AppRegistry } from 'react-native';
import App from './App';

AppRegistry.registerComponent('musee_mobile', () => App);
