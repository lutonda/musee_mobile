import React from 'react';
import {
  StyleSheet,
  Text,
  View, AsyncStorage, ActivityIndicator
} from 'react-native';
import { Router, Scene, Actions } from 'react-native-router-flux'
import { StackNavigator } from 'react-navigation'
import Authentication from './src/Pages/Authentication/Authentication';
import LoginScreen from './src/Pages/LoginScreen/LoginScreen'
import HomePage from './src/Pages/HomePage/HomePage';
import Splash from './src/Pages/Splash/Splash';
import { createStackNavigator, } from 'react-navigation';


class App extends React.Component {

  constructor() {
    super();
    this.state = { hasToken: false, isLoaded: true };
  }

  componentDidMount() {
    this._retrieveData().done();
  }
  _retrieveData = async () => {
    let value = await AsyncStorage.getItem('userData', (err, result) => {
      if (result !== null) {
        userData = JSON.parse(result)
        this.setState({ user: userData })
        Actions.HomePage({ user: userData })
      }
      else
        Actions.Authentication()
    });
  }
  _loadInitialState = async () => {

    value = await AsyncStorage.getItem('userData')
    if (value !== null)
      Actions.HomePage()
    return valuer
  }

  render() {
    if (!this.state.isLoaded) {
      return (
        <ActivityIndicator />
      )
    } else {
      return (

        <Router>
          <Scene key="root">
            <Scene key="Splash"
              component={Splash}
              animation='fade'
              hideNavBar={true}
              initial={true}
            />
            <Scene key="Authentication"
              component={Authentication}
              animation='fade'
              hideNavBar={true}
            />
            <Scene key="HomePage"
              component={HomePage}
              animation='fade'
              hideNavBar={true}
            />
          </Scene>
        </Router>
      )
    }
  }
}

export default App