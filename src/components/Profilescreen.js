import React, { Component } from 'react';
import {
    Image,
    View,
    Text,
    StyleSheet
} from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'
import { Button, Container, Header, Content, Left, Right } from 'native-base'

class ProfileScreen extends Component {

    static navigationOptions = {
        title: 'Perfil',
        drawerIcon: (<Icon name='user-circle' size={25} />)
    }

    render() {
        return (
            <Container>
                <Header style={{
                    backgroundColor: '#eee',
                    flexDirection: 'row',
                    flexWrap: 'wrap',
                    alignItems: 'flex-start',
                    textAlign: 'left',
                    height: 40
                }}>
                    <Left style={{ position: 'relative', left: 0, width: 100, alignSelf: 'flex-start' }}>
                        <Icon name='align-justify' style={{ padding: 5 }} size={25}
                            onPress={() => this.props.navigation.openDrawer()} />
                    </Left>
                    <Right>

                    </Right>
                </Header>
                <Content>
                    <Text onPress={() => this.props.navigation.navigate('DrawerOpen')}>Home Screen</Text>
                </Content>
            </Container>
        )
    }
}

export default ProfileScreen