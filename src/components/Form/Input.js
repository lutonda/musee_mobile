import React, { Component } from 'react';
import {
    StyleSheet, TextInput
} from 'react-native';
import { View,Text } from 'native-base';
import Validate from './Validate'

let key;
class Input extends Component {
    constructor(props) {
        super(props);
        this.state = { value: props.value, key: props.id, error: null };
        if (props.password)
            props.secureTextEntry = true
    }

    validate(key, value) {
        debugger
        //this.setState({erro:Validate(key, value)})
    }
    render() {
        return (
            <View>
                <TextInput
                    style={styles.textInput}
                    placeholderTextColor='rgba(255,255,255,0.7)'
                    underlineColorAndroid='transparent'
                    {...this.props}
                    onBlur={value => this.validate(this.state.key, value)}
                    onChangeText={val => this.props.onChangeText(this.state.key, val)}
                />
                <Text>{this.state.error}</Text>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    textInput: {
        alignSelf: 'stretch',
        marginBottom: 20,
        paddingHorizontal: 10,
        color: '#FFF',
        backgroundColor: 'rgba(255,255,255,0.3)',
    }
});
export default Input