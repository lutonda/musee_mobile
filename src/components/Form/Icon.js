import React, { Component } from 'react';
import {StyleSheet, Image} from 'react-native';
import { View, Text } from 'native-base';

let key;
class Icon extends Component {
    constructor(props) {
        super(props)
        this.state={
            key:'./../../assets/icons/color/'+this.props.name+'.png'
        }
    }

    render() {
        return (
            <Image source={{ uri:this.props.name}}/>
       );
    }
}

const styles = StyleSheet.create({});

export default Icon