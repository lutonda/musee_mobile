import React, { Component } from 'react';
import {
  Image,
  View,
  Text,
  StyleSheet, Alert, AsyncStorage
} from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'
import { Button, Container, Header, Content, Left, Right } from 'native-base'


import SettingsList from 'react-native-settings-list';

class SettingsScreen extends Component {
  constructor(props) {
    super(props);
    this.onValueChange = this.onValueChange.bind(this);
    this.state = { switchValue: false };
  }

  resetData() {
    Alert.alert(
      'Atenção',
      'Estas preste a apagar todos os seus dados Locais',
      [
        { text: 'Cancelar', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
        {
          text: 'OK, continuar', onPress: () => {
            AsyncStorage.removeItem('userData', function (e) {
              this.props.navigation.navigate('Homescreen')
            })
          }
        },
      ]
    )
  }
  static navigationOptions = {
    title: 'Definições',
      drawerIcon: (<Image source={require('./../../assets/icons/color/')} style={{width:28,height:28}}/>)
  }

  onValueChange(value) {
    this.setState({ switchValue: value });
  }
  render() {
    return (
      <Container>
        <Header style={{
          backgroundColor: '#eee',
          flexDirection: 'row',
          flexWrap: 'wrap',
          alignItems: 'flex-start',
          textAlign: 'left',
          height: 40
        }}>
          <Left style={{ position: 'relative', left: 0, width: 100, alignSelf: 'flex-start' }}>
            <Icon name='align-justify' style={{ padding: 5 }} size={25}
              onPress={() => this.props.navigation.openDrawer()} />
          </Left>
          <Right>

          </Right>
        </Header>
        <Content>
          <View style={{ backgroundColor: '#EFEFF4', flex: 1 }}>
            <View style={{ backgroundColor: '#EFEFF4', flex: 1 }}>
              <SettingsList borderColor='#c8c7cc' defaultItemSize={50}>
                <SettingsList.Header headerStyle={{ marginTop: 15 }} />
                <SettingsList.Item
                  icon={
                    <Image style={styles.imageStyle} source={require('../img/icons/SettingsPack/airplane.png')} />
                  }
                  hasSwitch={true}
                  switchState={this.state.switchValue}
                  switchOnValueChange={this.onValueChange}
                  hasNavArrow={false}
                  title='Airplane Mode'
                />
                <SettingsList.Item
                  icon={<Image style={styles.imageStyle} source={require('../img/icons/SettingsPack/wifi.png')} />}
                  title='Wi-Fi'
                  titleInfo='Bill Wi The Science Fi'
                  titleInfoStyle={styles.titleInfoStyle}
                  onPress={() => Alert.alert('Route to Wifi Page')}
                />
                <SettingsList.Item
                  icon={<Image style={styles.imageStyle} source={require('../img/icons/SettingsPack/blutooth.png')} />}
                  title='Blutooth'
                  titleInfo='Off'
                  titleInfoStyle={styles.titleInfoStyle}
                  onPress={() => Alert.alert('Route to Blutooth Page')}
                />
                <SettingsList.Item
                  icon={<Image style={styles.imageStyle} source={require('../img/icons/SettingsPack/cellular.png')} />}
                  title='Cellular'
                  onPress={() => Alert.alert('Route To Cellular Page')}
                />
                <SettingsList.Item
                  icon={<Image style={styles.imageStyle} source={require('../img/icons/SettingsPack/hotspot.png')} />}
                  title='Personal Hotspot'
                  titleInfo='Off'
                  titleInfoStyle={styles.titleInfoStyle}
                  onPress={() => Alert.alert('Route To Hotspot Page')}
                />
                <SettingsList.Header headerStyle={{ marginTop: 15 }} />
                <SettingsList.Item
                  icon={<Image style={styles.imageStyle} source={require('../img/icons/SettingsPack/notifications.png')} />}
                  title='Notifications'
                  onPress={() => Alert.alert('Route To Notifications Page')}
                />
                <SettingsList.Item
                  icon={<Image style={styles.imageStyle} source={require('../img/icons/SettingsPack/control.png')} />}
                  title='Control Center'
                  onPress={() => Alert.alert('Route To Control Center Page')}
                />
                <SettingsList.Item
                  icon={<Image style={styles.imageStyle} source={require('../img/icons/SettingsPack/dnd.png')} />}
                  title='Do Not Disturb'
                  onPress={() => Alert.alert('Route To Do Not Disturb Page')}
                />
                <SettingsList.Header headerStyle={{ marginTop: 15 }} />
                <SettingsList.Item
                  icon={<Image style={styles.imageStyle} source={require('../img/icons/SettingsPack/general.png')} />}
                  title='General'
                  onPress={() => Alert.alert('Route To General Page')}
                />
                <SettingsList.Item
                  icon={<Image style={styles.imageStyle} source={require('../img/icons/SettingsPack/display.png')} />}
                  title='Display & Brightness'
                  onPress={() => this.resetData()}
                />
              </SettingsList>
            </View>
          </View>
        </Content>
      </Container>
    )
  }
}

const styles = StyleSheet.create({
  imageStyle: {
    marginLeft: 15,
    marginRight: 20,
    alignSelf: 'center',
    width: 20,
    height: 24,
    justifyContent: 'center'
  }
});
export default SettingsScreen
/*
import SettingsList from 'react-native-settings-list';


class SettingsScreen extends Component {
  constructor() {
    super();
    this.onValueChange = this.onValueChange.bind(this);
    this.state = { switchValue: false };
  }

  static navigationOptions = {
    title: 'Perfil',
    drawerIcon: (<Icon name='user-circle' size={25} />)
  }
  resetData() {
    Alert.alert(
      'Atenção',
      'Estas preste a apagar todos os seus dados Locais',
      [
        { text: 'Cancelar', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
        { text: 'OK, continuar', onPress: () => { alert('OK Pressed') } },
      ]
    )
  }
  render() {
    var bgColor = '#DCE3F4';
    return (
      <View style={{ backgroundColor: '#EFEFF4', flex: 1 }}>
        <View style={{ borderBottomWidth: 1, backgroundColor: '#f7f7f8', borderColor: '#c8c7cc' }}>
          <Text style={{ alignSelf: 'center', marginTop: 30, marginBottom: 10, fontWeight: 'bold', fontSize: 16 }}>Settings</Text>
        </View>
        <View style={{ backgroundColor: '#EFEFF4', flex: 1 }}>
          <SettingsList borderColor='#c8c7cc' defaultItemSize={50}>
            <SettingsList.Header headerStyle={{ marginTop: 15 }} />
            <SettingsList.Item
              icon={
                <Image style={styles.imageStyle} source={require('../img/icons/SettingsPack/airplane.png')} />
              }
              hasSwitch={true}
              switchState={this.state.switchValue}
              switchOnValueChange={this.onValueChange}
              hasNavArrow={false}
              title='Airplane Mode'
            />
            <SettingsList.Item
              icon={<Image style={styles.imageStyle} source={require('../img/icons/SettingsPack/wifi.png')} />}
              title='Wi-Fi'
              titleInfo='Bill Wi The Science Fi'
              titleInfoStyle={styles.titleInfoStyle}
              onPress={() => Alert.alert('Route to Wifi Page')}
            />
            <SettingsList.Item
              icon={<Image style={styles.imageStyle} source={require('../img/icons/SettingsPack/blutooth.png')} />}
              title='Blutooth'
              titleInfo='Off'
              titleInfoStyle={styles.titleInfoStyle}
              onPress={() => Alert.alert('Route to Blutooth Page')}
            />
            <SettingsList.Item
              icon={<Image style={styles.imageStyle} source={require('../img/icons/SettingsPack/cellular.png')} />}
              title='Cellular'
              onPress={() => Alert.alert('Route To Cellular Page')}
            />
            <SettingsList.Item
              icon={<Image style={styles.imageStyle} source={require('../img/icons/SettingsPack/hotspot.png')} />}
              title='Personal Hotspot'
              titleInfo='Off'
              titleInfoStyle={styles.titleInfoStyle}
              onPress={() => Alert.alert('Route To Hotspot Page')}
            />
            <SettingsList.Header headerStyle={{ marginTop: 15 }} />
            <SettingsList.Item
              icon={<Image style={styles.imageStyle} source={require('../img/icons/SettingsPack/notifications.png')} />}
              title='Notifications'
              onPress={() => Alert.alert('Route To Notifications Page')}
            />
            <SettingsList.Item
              icon={<Image style={styles.imageStyle} source={require('../img/icons/SettingsPack/control.png')} />}
              title='Control Center'
              onPress={() => Alert.alert('Route To Control Center Page')}
            />
            <SettingsList.Item
              icon={<Image style={styles.imageStyle} source={require('../img/icons/SettingsPack/dnd.png')} />}
              title='Do Not Disturb'
              onPress={() => Alert.alert('Route To Do Not Disturb Page')}
            />
            <SettingsList.Header headerStyle={{ marginTop: 15 }} />
            <SettingsList.Item
              icon={<Image style={styles.imageStyle} source={require('../img/icons/SettingsPack/general.png')} />}
              title='General'
              onPress={() => Alert.alert('Route To General Page')}
            />
            <SettingsList.Item
              icon={<Image style={styles.imageStyle} source={require('../img/icons/SettingsPack/display.png')} />}
              title='Display & Brightness'
              onPress={() => this.resetData()}
            />
          </SettingsList>
        </View>
      </View>
    );
  }
  onValueChange(value) {
    this.setState({ switchValue: value });
  }
}


export default SettingsScreen

*/