import React, { Component } from 'react';
import{
Image,
View,
Text,
StyleSheet
} from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'

import {Button,Container,Header,Content,Left,Right} from 'native-base'
const DrawerHeader = ({ navigation }) => (
            <Header style={{backgroundColor:'#eee',flexDirection: 'row',flexWrap: 'wrap',alignItems:'flex-start',textAlign:'left', height:40}}>
                <Left style={{position: 'relative', left: 0,width: 100,alignSelf:'flex-start' }}>
                <Icon name='align-justify' style={{padding:5}} size={25} onPress={()=>this.props.navigation.openDrawer()}/>
                    </Left>
                    <Right>
                    
                        </Right>
                </Header>
    )

    export default DrawerHeader