import React, {Component} from 'react';
import {
    Image,
    View,
    Text,
    StyleSheet
} from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'
import {
    Button,
    Container,
    Content,
    Header,
    Left,
    Right,
    Title,
    Body
} from 'native-base'
import DrawerHeader from './DrawerHeader'

const Headernav = (navigation) => {
    return (
        <Header style={{
            backgroundColor: '#eee',
            flexDirection: 'row',
            flexWrap: 'wrap',
            alignItems: 'flex-start',
            textAlign: 'left',
            height: 40
        }}>
            <Left style={{position: 'relative', left: 0, width: 100, alignSelf: 'flex-start'}}>
                <Icon name='align-justify' style={{padding: 5}} size={25}
                      onPress={() => navigation.actions('openDrawer')}/>
            </Left>
            <Left>
                <Button transparent onPress={() => navigation.navigation.goBack()}>
                    <Icon name="ios-arrow-back"/>
                </Button>
            </Left>

            <Body>
            <Title>Blank page</Title>
            </Body>

            <Right>
                <Button
                    transparent
                    onPress={() => navigation.navigation.navigate("DrawerOpen")}
                >
                    <Icon name="ios-menu"/>
                </Button>
            </Right>
        </Header>
    )
}

export default Headernav