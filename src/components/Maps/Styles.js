import { StyleSheet } from 'react-native';

export const Styles = StyleSheet.create({
  container: {
    position:'absolute',
    top:0,
    left:0,
    bottom:0,
    right:0,
    justifyContent:'flex-end',
    alignItems: 'center',
  },
  map:{
    position:'absolute',
    top:0,
    left:0,
  }
});