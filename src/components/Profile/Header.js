import React, {Component} from 'react';
import {
    Image,
    View,
    Text,
    StyleSheet,
    ImageBackground,
    Left,
    Icon,
    Body
} from 'react-native'
export default class Header extends Component {
    render() {
        return (
            <View style={style.container}>
            <ImageBackground style={style.headerBackground} source={require('../../img/wp/walpaper_c.jpg')}>

            <Header style={{
                backgroundColor: 'transparent',
                flexDirection: 'row',
                flexWrap: 'wrap',
                alignItems: 'flex-start',
                textAlign: 'left',
                height: 40
            }}>
                <Left style={{position: 'relative', left: 0, width: 100, alignSelf: 'flex-start'}}>
                    <Icon name='align-justify' style={{padding: 5,color:'#fff'}} size={25}
                          onPress={() => this.props.navigation.openDrawer()}/>
                </Left>
                <Body/>
            </Header>
                <View style={style.header}>
                    <View style={style.profilepicwrap}>
                        <Image style={style.profilepic} source={{uri: base64}}/>
                    </View>
                    <Text style={style.name}>JOHN DOE</Text>
                    <Text style={style.pos}> - DEV - </Text>
                </View>
           </ImageBackground>
            </View>
        )
    }
}

const style = StyleSheet.create({
    headerBackground:{
        flex: 1,
        width:null,
        alignSelf: 'stretch',
    },
    header:{
        flex:1,
        alignItems: 'center',
        justifyContent: 'center',
        padding: 20,
        backgroundColor:'rgba(0, 0, 0, 0.5)',
    },
    profilepicwrap:{
        width:180,
        height:180,
        borderRadius: 100,
        borderColor: 'rgba(0,0,0,0.4)',
        borderWidth: 16,
    },
    profilepic:{
        flex:1,
        width:null,
        alignSelf: 'stretch',
        borderRadius:100,
        borderColor:  '#FFF',
        borderWidth:4,
    },
    name:{ 
        marginTop: 20,
        fontSize: 16,
        color:'#FFF',
        fontWeight: 'bold',
    },
    pos:{
        fontSize:14,
        color:'#0394c0',
        fontWeight:'300',
        fontStyle:'italic'

    }


})