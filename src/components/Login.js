import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  KeyboardAvoidingView,
  TouchableOpacity,
  AsyncStorage
} from 'react-native';
import {Actions} from 'react-native-router-flux';
import { StackNavigator } from 'react-navigation'

let value
class Login extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      username: '',
      password: ''
    }
  }

  async saveItem(item, selectedValue) {
    try {
        await AsyncStorage.setItem(item, selectedValue);
    } catch (error) {
        console.error('AsyncStorage error: ' + error.message);
    }
}
  onChangeText = (key, value) => {
    this.setState({
      [key]: value
    })
  }
  componentDidMount() {
    this._loadInitialState().done();
  }
  
  _loadInitialState = async () => {
    
    value = null//AsyncStorage.getItem('user');
    if (value !== null)
      this.props.navigation.navigate('Profile')
  }
  login = () => {
    try {

      fetch('http://192.168.100.3:8000/api/Rest/mus/verificar/utilizador', {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'content-Type': 'application/json',
          'apikey': 'newapikeyforuseb',
          'X-AUTH-TOKEN': 123,

        },
        body: JSON.stringify({
          username: this.state.username,
          password: this.state.password
        })
      })
        .then((response) => response.json())
        .then((res) => {
          if (typeof (res.username) !== 'undefined') {
            this.saveItem('user', res),
            Actions.Profile()
            //this.props.navigation.navigate('Profile')
          }
          else {
            alert('Você digitou um nome de usuário ou senha inválidos')
            this.setState({ username: '', password: '' })
          }
        })
        .catch((error) => {
          console.error(error);
        }).done()

    } catch (error) {
      alert('Não foi possivel conectar-se ao servidor. certifique que tens uma concção a Internte e volte a tentar')
    }
  }
  render() {
    return (
      <KeyboardAvoidingView behavior='padding' style={style.wraper}>
        <View style={style.container}>
          <View style={style.logoContainer}>
            <Image style={style.logo} source={require('../img/logo/logo_mini_dark.png')} />
            <Text style={style.title}>Museu de Geologia</Text>
          </View>
          <View style={style.formContainer}>
            <TextInput
              style={style.textInput}
              placeholder='Username'
              placeholderTextColor='rgba(255,255,255,0.7)'
              value={this.state.username}
              onChangeText={val => this.onChangeText('username', val)}
              underlineColorAndroid='transparent'
            ></TextInput>

            <TextInput
              style={style.textInput}
              placeholder='Password'
              placeholderTextColor='rgba(255,255,255,0.7)'
              value={this.state.password}
              onChangeText={val => this.onChangeText('password', val)}
              underlineColorAndroid='transparent'
              secureTextEntry
              password='true'
            ></TextInput>

            <TouchableOpacity style={style.btn} onPress={this.login}>
              <Text>Login</Text>
            </TouchableOpacity>
          </View>

        </View>
      </KeyboardAvoidingView>
    );
  }
}

const style = StyleSheet.create({
  wraper: {
    flex: 1
  },
  container: {
    flex: 1,
    backgroundColor: '#0680f9',
    padding: 20
  },
  logo: {
    width: 100,
    height: 100
  },
  logoContainer: {
    alignItems: 'center',
    flexGrow: 1,
    justifyContent: 'center',
    margin: 20,
  },
  title: {
    color: '#FFF',
    marginTop: 10,
    width: 200,
    textAlign: 'center',

  },

  textInput: {
    alignSelf: 'stretch',
    marginBottom: 20,
    paddingHorizontal: 10,
    color: '#FFF',
    backgroundColor: 'rgba(255,255,255,0.3)',
  },
  btn: {
    alignSelf: 'stretch',
    backgroundColor: '#034d96',
    padding: 20,
    alignItems: 'center'

  }

})
export default Login