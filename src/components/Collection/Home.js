import React, {PureComponent} from 'react';
import {
    Image,
    View,
    Text,
    StyleSheet,
    ImageBackground
} from 'react-native'

import { TabViewAnimated, TabBar, SceneMap } from 'react-native-tab-view'; // 0.0.69
import Icon from 'react-native-vector-icons/FontAwesome'
import {Button, Container, Header, Content, Left, Right} from 'native-base'


import Collection from './TabNavigator';

class Home extends PureComponent {
    constructor(props) {
        super(props);

        this.state = { loading: true, GridViewItems: [] }
        fetch('http://192.168.100.3:8000/api/Rest/todosacervos', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'content-Type': 'application/json',
                'apikey': 'newapikeyforuseb',
                'X-AUTH-TOKEN': 123,

            },
            body: JSON.stringify({ categoria: 'mineralogia' })
        })
            .then((response) => response.json())
            .then((res) => {
                debugger
                if (res != null) {
                    this.setState({ GridViewItems: res })
                }
                else {
                    Alert.alert('Erro');
                }
            })
            .catch((error) => {
                Alert.alert('Erro');
            }).done(() => {
                this.setState({ loading: false })
            })
    }
    static navigationOptions = {
        title: 'Perfil',
        drawerIcon: (<Icon name='user-circle' size={25}/>)
    }
    render() {
        return (
            <Container>
                <Header style={{
                    backgroundColor: '#eee',
                    flexDirection: 'row',
                    flexWrap: 'wrap',
                    alignItems: 'flex-start',
                    textAlign: 'left',
                    height: 40
                }}>
                    <Left style={{position: 'relative', left: 0, width: 100, alignSelf: 'flex-start'}}>
                        <Icon name='align-justify' style={{padding: 5}} size={25}
                              onPress={() => this.props.navigation.openDrawer()}/>
                    </Left>
                    <Right>

                    </Right>
                </Header>
                <Collection  {...this.props}/>
            </Container>
        )
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
    },
  });
export default Home