import React, {PureComponent} from 'react';
import {
    Image,
    View,
    Text,
    StyleSheet
} from 'react-native'

import { TabViewAnimated, TabBar, SceneMap } from 'react-native-tab-view'; // 0.0.69
import Icon from 'react-native-vector-icons/FontAwesome'
import {Button, Container, Header, Content, Left, Right} from 'native-base'


import Collection from './TabNavigator';
const FirstRoute = () => (<Thumbnail navigation={this.props.navigation}/>);
const SecondRoute = () => (<List onPress={this.props} />);

class Home extends PureComponent {
    constructor(props) {
        super(props);
        
    }
    static navigationOptions = {
        title: 'Acervo',
        drawerIcon: (<Icon name='archive' size={25}/>)
    }
    render() {
        return (
            <Container>
                <Header style={{
                    backgroundColor: '#eee',
                    flexDirection: 'row',
                    flexWrap: 'wrap',
                    alignItems: 'flex-start',
                    textAlign: 'left',
                    height: 40
                }}>
                    <Left style={{position: 'relative', left: 0, width: 100, alignSelf: 'flex-start'}}>
                        <Icon name='align-justify' style={{padding: 5}} size={25}
                              onPress={() => this.props.navigation.openDrawer()}/>
                    </Left>
                    <Right>

                    </Right>
                </Header>
                <Collection  {...this.props}/>
            </Container>
        )
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
    },
  });
export default Home