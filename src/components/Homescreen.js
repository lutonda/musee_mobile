import React, {Component} from 'react';
import {
    Image,
    View,
    Text,
    StyleSheet,
    AsyncStorage,
    ImageBackground,
    ScrollView,
    Dimensions
} from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'
import {
    Button,
    Container,
    Content,
    Header,
    Left,
    Right,
    Title,
    Body	} from 'native-base'

var data,base64
class HomeScreen extends Component {
    constructor(props){
        super(props)
        this.state = {
            user: '',
            data: '',
            base64:''
        }
    
    }
    componentDidMount() {
        this._loadInitialState().done();
    }



    _loadInitialState = async () => {
        data = await AsyncStorage.getItem('data');
         data = JSON.parse(data)
        //  base64=data.pessoa.pessoa.files[0].data64  
        //  this.setState({base64:base64})      
    }
    static navigationOptions = {
        title: 'Inicio',
        drawerIcon: (<Icon name='home' size={25}/>)
    }

    render() {
        return (
            <Container>
            <View style={style.container}>
            <ImageBackground style={style.headerBackground} source={require('../img/wp/walpaper_c.jpg')}>

            <Header style={{
                backgroundColor: 'transparent',
                flexDirection: 'row',
                flexWrap: 'wrap',
                alignItems: 'flex-start',
                textAlign: 'left',
                height: 40
            }}>
                <Left style={{position: 'relative', left: 0, width: 100, alignSelf: 'flex-start'}}>
                    <Icon name='align-justify' style={{padding: 5,color:'#fff'}} size={25}
                          onPress={() => this.props.navigation.openDrawer()}/>
                </Left>
                <Body/>
            </Header>
                <View style={style.header}>
                    <View style={style.profilepicwrap}>
                        <Image style={style.profilepic} source={{uri: base64}}/>
                    </View>
                    <Text style={style.name}>JOHN DOE</Text>
                    <Text style={style.pos}> - DEV - </Text>
                </View>
           </ImageBackground>
            </View>
            <View style={style.bar}>
                <View style={[style.barItem,style.barseparator]}>
                    <Text style={style.barTop}>552</Text>
                    <Text style={style.barBottom}>Acervos</Text>
                </View>
                <View style={[style.barItem,style.barseparator]}>
                    <Text style={style.barTop}>750</Text>
                    <Text style={style.barBottom}>Eventos</Text>
                </View>
            </View>
            <ScrollView style={style.scrollphotoGrid}>
                <View style={style.photoGrid}>
                    <View style={style.photoWrap}>
                        <Image style={style.photo} source={require('../img/wp/walpaper_e.jpg')}/>
                    </View>
                    <View style={style.photoWrap}>
                        <Image style={style.photo} source={require('../img/wp/walpaper_d.jpg')}/>
                    </View>
                    <View style={style.photoWrap}>
                        <Image style={style.photo} source={require('../img/wp/walpaper_b.jpg')}/>
                    </View>
                    <View style={style.photoWrap}>
                        <Image style={style.photo} source={require('../img/wp/walpaper_c.jpg')}/>
                    </View>
                    <View style={style.photoWrap}>
                        <Image style={style.photo} source={require('../img/wp/walpaper_b.jpg')}/>
                    </View>
                    <View style={style.photoWrap}>
                        <Image style={style.photo} source={require('../img/wp/walpaper_e.jpg')}/>
                    </View>
                    <View style={style.photoWrap}>
                        <Image style={style.photo} source={require('../img/wp/walpaper_b.jpg')}/>
                    </View>
                    <View style={style.photoWrap}>
                        <Image style={style.photo} source={require('../img/wp/walpaper_c.jpg')}/>
                    </View>
                    <View style={style.photoWrap}>
                        <Image style={style.photo} source={require('../img/wp/walpaper_e.jpg')}/>
                    </View>
                </View>
            </ScrollView>
            </Container>
        )
    }
}

const style = StyleSheet.create({
    container:{
        flex:1,
        backgroundColor:'#000'
    },
    
    headerBackground:{
        flex: 1,
        width:null,
        alignSelf: 'stretch',
    },
    header:{
        flex:1,
        alignItems: 'center',
        justifyContent: 'center',
        padding: 30,
        backgroundColor:'rgba(0, 0, 0, 0.5)',
    },
    profilepicwrap:{
        width:180,
        height:180,
        borderRadius: 100,
        borderColor: 'rgba(0,0,0,0.4)',
        borderWidth: 16,
    },
    profilepic:{
        flex:1,
        width:null,
        alignSelf: 'stretch',
        borderRadius:100,
        borderColor:  '#FFF',
        borderWidth:4,
    },
    name:{ 
        marginTop: 18,
        fontSize: 20,
        color:'#FFF',
        fontWeight: 'bold',
    },
    pos:{
        fontSize:14,
        color:'#0394c0',
        fontWeight:'300',
        fontStyle:'italic',
        marginBottom: 10,

    },

    bar:{
        
        borderWidth: 4,
        backgroundColor: '#0394c0',
        flexDirection:'row',
        marginLeft: 50,
        marginRight: 50,
        borderRadius: 50,
        marginTop:-30,
        marginBottom:-70,
         zIndex: 1 
    },
    barseparator:{
    },
    barItem:{
        flex:1,
        padding:5,
        alignItems: 'center',
        borderColor:'#FFF',
        margin: 2,
    },
    barTop:{
        color: '#FFF',
        fontSize:18,
        fontWeight: 'bold',
        fontStyle:'italic'
    },
    barBottom:{
        color: '#000',
        fontSize:14,
        fontWeight: 'bold',

    },
    scrollphotoGrid:{
        marginTop: 35,},

    photoGrid:{
        flexDirection:'row',
        flexWrap:'wrap'
    },
    photoWrap:{
        margin:2,
        height:120,
        width:(Dimensions.get('window').width/3)-4
    },
    photo:{
        flex:1,
        width:null,
        alignSelf: 'stretch',
    }
    
})
/*
const style = StyleSheet.create({
    container: {},
    header: {
        alignContent: 'center',
        alignItems: 'center',
        height: 250,
        backgroundColor: '#409fbf'
    }, body: {
        flex: 1,
        justifyContent: 'center',
        alignContent: 'center',
        alignItems: 'center',
    },
    avatar: {
        justifyContent: 'center',
        alignContent: 'center',
        alignItems: 'center',
        height: 150,
        width: 150,
        margin:5,
        borderRadius: 75,
        borderWidth: 1,
        borderColor: '#FFF',
    },
    username:{
        fontSize:26,
        fontWeight:'900',
        color:'#FFF'
    }
})
*/

export default HomeScreen