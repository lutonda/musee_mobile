import React, { Component } from 'react';
import {
    Image,
    StyleSheet,
    Text,
    View,
    AsyncStorage,
    TouchableOpacity,
    ImageBackground
} from 'react-native';

import { DrawerNavigator, DrawerItems } from 'react-navigation'
import Homescreen from './Homescreen'
import SettingsScreen from './SettingsScreen'
import ProfileScreen from './Profilescreen';
import Appointment from './AppointmentBook/Appointment';
import Home from './Collection/Index';
import Item from './Collection/Item';

import Evento from './AppointmentBook/Evento'
import { Container, Header, Body, Content, Drawer, Icon, Label } from 'native-base';
import Index from './Collection/Index';
const base64 = ''
let user = ''
let data = ''
export default class Profile extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            user: '',
            data: '',
            avatar: ''
        }
    }

    async saveItem(item, selectedValue) {
        try {
            await AsyncStorage.setItem(item, selectedValue);
        } catch (error) {
            console.error('AsyncStorage error: ' + error.message);
        }
    }
    componentDidMount() {
        this._loadInitialState().done();
    }


    _loadInitialState = async () => {
        var value = await AsyncStorage.getItem('user');
        if (value === null)
            this.props.navigation.navigate('Login')
        else {
            value = await JSON.parse(value)
            this.setState({ user: value })

            var data = await AsyncStorage.getItem('data');
            /** ------------------------ ***/
            if (data !== null) {
                data = JSON.parse(data)
                base64 = data.pessoa.pessoa.files[0].data64
            }
            else
                fetch('http://192.168.100.3:8000/api/Rest/mus/utilizador/show', {
                    method: 'POST',
                    headers: {
                        'Accept': 'application/json',
                        'content-Type': 'application/json',
                        'apikey': 'newapikeyforuseb',
                        'X-AUTH-TOKEN': 123,

                    },
                    body: JSON.stringify({
                        'U-TOKEN': value.username
                    })
                })
                    .then((response) => response.json())
                    .then((res) => {
                        
                        if (res != null) {
                            AsyncStorage.setItem('data', res)
                            base64 = res.pessoa.pessoa.files[0].data64
                        }
                        else {
                            alert('Erro')
                        }
                    })
                    .catch((error) => {
                    }).done()

            /** ------------------------ ***/
        }
    }

    async userLogout() {
        try {
          await AsyncStorage.removeItem('user');
          Alert.alert('Logout Success!');
          Actions.Login();
        } catch (error) {
          console.log('AsyncStorage error: ' + error.message);
        }
      }
    render() {
        return (<MyApp />);
    }
}
const CostumerDrawerContentComponent = (props) => (
    <Container style={{ backgroundColor: '#eee' }}>
        <Header style={style.header}>
            <ImageBackground style={style.headerBackground} source={require('../img/wp/walpaper_d.jpg')}>
                <Body style={{ flexDirection: "row", textAlignVertical: 'bottom' }}>

                    <Image style={style.avatar} source={{ uri: base64 }} />
                    <View style={{ alignSelf: 'flex-end', margin: 5 }}>
                        <Text style={style.username}></Text>
                        <Text style={{ color: '#FFF' }}>Lutonda</Text>
                    </View>
                </Body>
            </ImageBackground>
        </Header>
        <Content>
            <DrawerItems {...props}
                onItemPress={({ route, focused }) => { props.onItemPress({ route, focused }) }} />
        </Content>

        <TouchableOpacity onPress={this._logout} style={{ buttom: 0, padding: 20 }}>
            <Text>Sair</Text>
        </TouchableOpacity>
    </Container>
)

const MyApp = DrawerNavigator({
    Homescreen: { screen: Homescreen },
    Home: { screen: Home },
    Appointment: { screen: Appointment },
    Profile: { screen: ProfileScreen },
    Settings: { screen: SettingsScreen },
    Item: {
        screen: Item, navigationOptions: {
            drawerLabel: () => null
        }
    },
    Evento: {
        screen: Evento, navigationOptions: {
            drawerLabel: () => null
        }
    }
}, {
        initialRouteName: 'Homescreen',
        headerMode: 'screen',
        contentComponent: CostumerDrawerContentComponent,
        drawerOpenRoute: 'DrawerOpen',
        drawerCloseroute: 'DrawerClose',
        drawerToggleRoute: 'DrawerToggle'
    })
const style = StyleSheet.create({
    container: {},
    header: {
        height: 200,
        backgroundColor: '#409fbf'
    },
    headerBackground: {
        width: 300
    }, body: {
        flex: 1,
        justifyContent: 'center',
        alignContent: 'center',
        alignItems: 'center',
    },
    avatar: {
        height: 80,
        width: 80,
        borderRadius: 75,
        borderWidth: 2,
        borderColor: '#FFF',
        margin: 5,
        alignSelf: 'flex-end'

    },
    username: {
        fontSize: 18,
        fontWeight: '900',
        color: '#FFF'
    }
})