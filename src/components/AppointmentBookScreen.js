import React, { Component } from 'react';
import{
    Image,
View,
Text,
StyleSheet
} from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'
import {Button,Container,Header,Content,Left, Tab} from 'native-base'

import {createStackNavigator, createBottomTabNavigator} from 'react-navigation'
import TabNavigator from './AppointmentBook/TabNavigator';

class AppointmentBookScreen extends Component{

    static navigationOptions={
        title:'Agenda',
        drawerIcon:(<Icon name='calendar' size={25}/>)
    }
render(){
    return(
        <TabNavigator/>
    )
}
}

export default AppointmentBookScreen