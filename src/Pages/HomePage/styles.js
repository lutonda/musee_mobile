import {StyleSheet} from 'react-native'

const styles = StyleSheet.create({
    container: {},
    header: {
      height: 200,
      backgroundColor: '#409fbf'
    },
    headerBackground: {
      width: 300
    }, body: {
      flex: 1,
      justifyContent: 'flex-end',
      alignContent: 'flex-end',
      alignItems: 'flex-end',
    },
    avatar: {
      height: 100,
      width: 100,
      borderRadius: 75,
      borderWidth: 2,
      borderColor: '#FFF',
      margin: 5,
      alignSelf: 'flex-end',
    },
    username: {
      fontSize: 18,
      fontWeight: '900',
      color: '#FFF'
    }
  })
  export default styles