import React, { Component } from 'react';
import {
  Image,
  StyleSheet,
  Text,
  View,
  AsyncStorage,
  TouchableOpacity,
  ImageBackground
} from 'react-native';

import { Actions } from 'react-native-router-flux';
import { DrawerNavigator, DrawerItems } from 'react-navigation'
import Homescreen from '../Homescreen/Homescreen'
import SettingsScreen from '../SettingsScreen/SettingsScreen'
import ProfileScreen from '../ProfileScreen/ProfileScreen';
import AppointmentBookScreen from '../AppointmentBookScreen/AppointmentBookScreen';
import CollectionScreen from '../CollectionScreen/CollectionScreen';
import Item from '../../components/Collection/Item';

import Evento from '../../components/AppointmentBook/Evento'
import { Container, Header, Body, Content, Drawer, Icon, Label } from 'native-base';

import styles from './styles';
let user;
let avatar;
class HomePage extends Component {
  constructor(props) {
    super(props);
    user = this.props.user
    avatar=this.props.user.pessoa.files[0].data64
  }
  render() {
    return (<MyApp />);
  }
}
const CostumerDrawerContentComponent = (props) => (
  <Container style={{ backgroundColor: '#eee' }}>
    <Header style={styles.header}>
      <ImageBackground style={styles.headerBackground} source={require('../../img/wp/walpaper_d.jpg')}>
        <Body style={{ flex: 1, flexDirection: "row", textAlignVertical: 'bottom' }}>
          <Image style={styles.avatar} source={{ uri: user.pessoa.files[0].data64 }} />
          <View style={{ alignSelf: 'flex-end', margin: 5, }}>
            <Text style={styles.username}>{user.pessoa.nome}</Text>
            <Text style={{ color: '#FFF' }}>{user.username}</Text>
          </View>
        </Body>
      </ImageBackground>
    </Header>
    <Content style={{backgroundColor:'#333'}}>
      <DrawerItems {...props}
        onItemPress={({ route, focused }) => { props.onItemPress({ route, focused }) }} />
    </Content>

    <TouchableOpacity onPress={()=>AsyncStorage.removeItem('userData', () => {Actions.Authentication()})} style={{ buttom: 0, padding: 20 }}>
      <Text>Sair</Text>
    </TouchableOpacity>
  </Container>
)

const MyApp = DrawerNavigator({
  Homescreen: { screen: Homescreen },
  CollectionScreen: { screen: CollectionScreen },
  AppointmentBookScreen: { screen: AppointmentBookScreen },
  Profile: { screen: ProfileScreen },
  Settings: { screen: SettingsScreen },
  Item: {
    screen: Item, navigationOptions: {
      drawerLabel: () => null
    }
  },
  Evento: {
    screen: Evento, navigationOptions: {
      drawerLabel: () => null
    }
  }
}, {
    initialRouteName: 'Homescreen',
    headerMode: 'screen',
    contentComponent: CostumerDrawerContentComponent,
    drawerOpenRoute: 'DrawerOpen',
    drawerCloseroute: 'DrawerClose',
    drawerToggleRoute: 'DrawerToggle'

  })


export default HomePage;