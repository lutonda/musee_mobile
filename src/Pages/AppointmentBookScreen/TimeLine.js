import React, { Component } from 'react';
import {
    View,
    StyleSheet,
    Image,
    Text,
    ScrollView,
    ImageBackground,
    Dimensions
} from 'react-native'
import EventCalendar from 'react-native-events-calendar'

import Timeline from 'react-native-timeline-listview'

import style from './style'

let { width } = Dimensions.get('window')

var Eventos = []
class TimeLine extends Component {
   
constructor(){
    super()
    this.data = [
      {time: '2018-07-30 20:00', title: 'Event 1', description: 'Event 1 Description'},
      {time: '10:45', title: 'Event 2', description: 'Event 2 Description'},
      {time: '12:00', title: 'Event 3', description: 'Event 3 Description'},
      {time: '14:00', title: 'Event 4', description: 'Event 4 Description'},
      {time: '16:30', title: 'Event 5', description: 'Event 5 Description'}
    ]
  }

render(){
    return(
        <Timeline
          data={this.data}
        />
    )
}
}

export default TimeLine