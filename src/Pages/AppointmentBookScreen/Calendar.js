import React, { Component } from 'react';
import {
    View,
    StyleSheet,
    Image,
    Text,
    ScrollView,
    ImageBackground,
    Dimensions,
    TouchableOpacity
} from 'react-native'
import Moment from 'moment';

import { Agenda, LocaleConfig } from 'react-native-calendars';

var Eventos = {}
class Calendario extends Component {
    constructor(props) {
        super(props)
        this.props.eventos.map((evento) => {
            Moment.locale('en');
            var date = new Date(evento.dataAgenda.timestamp * 1000);
            data = Moment(date).format('YYYY-MM-DD')
            if (Eventos.hasOwnProperty(data))
                Eventos[data].push(evento)
            else
                Eventos[data] = [evento]
        })
        this.state = {
            eventos: Eventos
        }
        LocaleConfig.locales['pt'] = {
            monthNames: ['Janeiro', 'fevereiro', 'Março', 'April', 'Mai', 'junho', 'Julho', 'Agosto', 'setembro', 'outubro', 'November', 'Dezembro'],
            monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun','Jul.','Ago','Set.','Oct.','Nov.','Dez.'],
            dayNames: ['Domingo, Segunda, Terça, Quarta, Quinta, Sexta, Sábado'],
            dayNamesShort: ['Dom.','Seg.','Ter.','Qua.','Qui.','Sex.','Sab.']
          };
          
          LocaleConfig.defaultLocale = 'pt';
    }

    _eventTapped(item) {
        this.props.navigation.navigate('Evento', { item: item })
    }


    render() {
            const { navigate } = this.props.navigation;
        return (
            <View style={{ flex: 1, height: '100%' }}>
                <Agenda
                    items={Eventos}
                    renderItem={(item, firstItemInDay) => {
                        return (
                            <TouchableOpacity style={styles.container} onPress={()=>this._eventTapped(item)}>
                                <Text>{item.horainicial} - {item.horafinal}</Text>
                                <Text style={styles.title}>{item.idevento.titulo}</Text>
                                <Text style={{ color: '#777' }}>{item.idevento.descricao.substring(1, 200)}</Text>
                            </TouchableOpacity>
                        );
                    }}
                    renderEmptyDate={() => { return (<View />); }}
                    renderEmptyData={() => { return (<View />); }}
                    rowHasChanged={(r1, r2) => { return r1.text !== r2.text }}
                />

            </View>
        )
    }
}
const styles = StyleSheet.create({

    MainContainer: {
        backgroundColor: '#FFF',
        margin: 10,
    },
    container: {
        padding: 15,
        margin:10,
        borderRadius: 3,
        backgroundColor: '#FFF',
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 5 },
        shadowOpacity: 0.8,
        shadowRadius: 2,
        elevation: 1,
    },
    title: {
        fontSize: 22,
        backgroundColor: '#f0f0f0',
        marginLeft:-15,
        marginRight:-15,
        padding:10 
    },
});

export default Calendario