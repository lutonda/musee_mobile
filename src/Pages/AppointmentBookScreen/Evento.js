import React, { Component } from 'react';
import {
    View,
    StyleSheet,
    Image,
    Text,
    ScrollView,
    ImageBackground,
    TouchableOpacity
} from 'react-native'

import Moment from 'moment';
import Icon from 'react-native-vector-icons/FontAwesome'
import { Button, Container, Content, Header, Left, Right } from 'native-base'
var item
class Evento extends Component {
    constructor(props) {
        super(props)
        Moment.locale('pt');
        item = this.props.navigation.state.params.item
    }
    item = this.props.navigation.state.params.item
    render() {
        return (
            <View style={{ height: '100%' }}>
                <Header style={{ backgroundColor: '#eee', flexDirection: 'row', flexWrap: 'wrap', alignItems: 'flex-start', textAlign: 'left', height: 40 }}>
                    <Left style={{ position: 'relative', left: 0, width: 100, alignSelf: 'flex-start' }}>
                        <Icon name='chevron-left' style={{ padding: 5 }} size={25} onPress={() => this.props.navigation.goBack()} />
                    </Left>
                    <Right></Right>
                </Header>
                <View style={{ flex: 1, flexDirection: 'column' }}>
                    <ImageBackground style={{ flex: 1, justifyContent: 'flex-end' }} source={require('../../img/bg/bg_2.jpg')} >
                        <Text style={styles.title}>{item.idevento.titulo}</Text>
                        <View style={{ height: 144, width: 100, alignSelf: 'flex-end', bottom: -100, right: 5, zIndex: 1, backgroundColor: '#000' }}>
                            <Text style={{ color: '#FFF', fontSize: 68, textAlign: 'center' }}>{Moment(new Date(item.dataAgenda.timestamp * 1000)).format('DD')}</Text>
                            <Text style={{ color: '#FFF', textAlign: 'center' }}>{Moment(new Date(item.dataAgenda.timestamp * 1000)).format('MMMM YYYY')}</Text>
                            <View style={{ backgroundColor: '#333' }}><Text style={{ color: '#FFF', textAlign: 'center' }}>{item.horainicial} - {item.horafinal}</Text></View>
                        </View>
                    </ImageBackground>
                    <View style={{ backgroundColor: '#f0f0f0', padding: 5, paddingLeft: 20 }}>
                        <Text>por: {item.orador.nome}</Text>
                    </View>
                    <View style={{backgroundColor: '#034d96', padding: 5, paddingLeft: 20,flexDirection:'row' }}>
                        <Icon name='map-marker' size={20} style={{color:'#FFF'}}/>
                        <Text style={{color:'#ffffff'}}>{item.idlocal.nome}, {item.idlocal.descricao}</Text>
                    </View>
                    <ScrollView style={{ flex: 1, backgroundColor: '#FFF', padding: 25 }}>
                        <Text>{item.idevento.descricao}</Text>
                    </ScrollView>
                    <View style={{ height: 65, backgroundColor: '#f0f0f0', alignContent: 'center', alignItems: 'center' }}>
                        <TouchableOpacity style={styles.btn}><Text style={{ color: '#FFF' }}>Inscrever-se</Text></TouchableOpacity>
                    </View>
                </View>
            </View>
        );
    }
}
const styles = StyleSheet.create({

    MainContainer: {
        backgroundColor: '#FFF',
        margin: 10,
    },
    container: {
        flex: 1,
        flexDirection: 'row',
        marginBottom: 1,
        borderBottomColor: '#eee',
        height: '100%'
    },
    title: {
        fontSize: 42,
        padding: 10,
        color: '#FFF',
        backgroundColor: 'rgba(0,0,0,.3)',
        textShadowColor: 'rgba(0, 0, 0, 0.75)',
        textShadowOffset: { width: -1, height: 1 },
        textShadowRadius: 10,
        alignSelf: 'flex-end',
        bottom: 2
    },
    value: {
        fontSize: 18,
        fontWeight: '600',
        justifyContent: 'center',
        alignItems: 'center',
        padding: 9,
        width: null
    },
    btn: {
        alignSelf: 'stretch',
        backgroundColor: '#034d96',
        padding: 20,
        alignItems: 'center',
        borderRadius: 3,
        padding: 15,
        maxWidth: 200,
        margin: 5,

    }
});
export default Evento