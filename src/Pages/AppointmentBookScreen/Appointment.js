import React, { Component } from 'react';
import {
    View, StyleSheet, Dimensions, Image, Text,
    ScrollView,
    TouchableWithoutFeedback,
    AppRegistry, FlatList, Alert, Platform,
    AsyncStorage,
    ImageBackground,MapView
} from 'react-native'

import {createStackNavigator} from 'react-navigation';

import Icon from 'react-native-vector-icons/FontAwesome'
import { Button, Container, Content, Header, Left, Right } from 'native-base'
import { TabView, TabBar, SceneMap } from 'react-native-tab-view';
import TabNavigator from 'react-native-tab-navigator';

import Calendario from './Calendar'
import TimeLine from './TimeLine'
import List from './List'

const resizeMode = 'center';
const { width } = Dimensions.get('window');
const height = width * 0.5;
import Evento from './Evento'
import Loader from '../Loader'
const App = createStackNavigator({
    Evento: { screen: Evento }
  });
class Appointment extends Component {

    constructor(props) {
        super(props);
        this.state = { index: 0,selectedTab: 'timeline', loading: true, eventos: []}
        fetch('http://192.168.100.3:8000/api/Rest/mev/eventos/proximasagendas', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'content-Type': 'application/json',
                'apikey': 'newapikeyforuseb',
                'X-AUTH-TOKEN': 123,

            }
        })
            .then((response) => response.json())
            .then((res) => {
                
                if (res != null) {
                    this.setState({ eventos: res })
                }
                else {
                    Alert.alert('Erro');
                }
            })
            .catch((error) => {
                Alert.alert('Erro');
            }).done(() => {
                this.setState({ loading: false })
            })
    }

    static navigationOptions={
        title:'Agenda',
        drawerIcon: (<Image source={require('./../../assets/icons/color/calendario.png')} style={{width:28,height:28}}/>)
    }
    _handleMapRegionChange = mapRegion => {
        this.setState({ mapRegion });
    };
    render() {
        const resizeMode = 'center';
        return (

            <View style={{ height: '100%' }}>
                <Loader loading={this.state.loading} />
                <Header style={{ backgroundColor: '#eee', flexDirection: 'row', flexWrap: 'wrap', alignItems: 'flex-start', textAlign: 'left', height: 40 }}>
                    <Left style={{ position: 'relative', left: 0, width: 100, alignSelf: 'flex-start' }}>
                        <Icon name='align-justify' style={{ padding: 5 }} size={25} onPress={() => this.props.navigation.openDrawer()} />
                    </Left>
                    <Right><Text style={{ fontSize: 24 }}></Text></Right>
                </Header>
                <ImageBackground style={{ flex: 1 }} source={require('../../img/bg/bg_2.jpg')}>
                    <TabNavigator style={{ height: '100%' }}
                        tabBarStyle={{ backgroundColor: '#FFF', height: 62 }}
                        sceneStyle={{ paddingBottom: 60 }}>
                        <TabNavigator.Item
                            selected={this.state.selectedTab === 'timeline'}
                            title="Linha de Tempo"
                            renderIcon={() => <Image source={require('../../img/icons/006-puzzle.png')} style={{ height: 30, width: 30, }} />}
                            onPress={() => this.setState({ selectedTab: 'timeline' })}>
                            <TimeLine/>
                        </TabNavigator.Item>
                        <TabNavigator.Item
                            selected={this.state.selectedTab === 'calendar'}
                            title="Calendario"
                            renderIcon={() => <Image source={require('../../img/icons/005-event.png')} style={{ height: 30, width: 30, }} />}
                            onPress={() => this.setState({ selectedTab: 'calendar' })}>
                            <Calendario navigation={this.props.navigation} eventos={this.state.eventos} style={{height:'100%', backgroundColor:'#0CF'}}/>
                        </TabNavigator.Item>
                        <TabNavigator.Item
                            selected={this.state.selectedTab === 'lista'}
                            title="Lista"
                            renderIcon={() => <Image source={require('../../img/icons/018-clipboard-1.png')} style={{ height: 30, width: 30, }} />}
                            onPress={() => this.setState({ selectedTab: 'lista' })}>
                            <List eventos={this.state.eventos}/>
                        </TabNavigator.Item>
                        <TabNavigator.Item
                            selected={this.state.selectedTab === 'mine'}
                            title="Meus Eventos"
                            renderIcon={() => <Image source={require('../../img/icons/018-clipboard-1.png')} style={{ height: 30, width: 30, }} />}
                            onPress={() => this.setState({ selectedTab: 'mine' })}>
                            <Text>Meus eventos</Text>
                        </TabNavigator.Item>
                    </TabNavigator>
                </ImageBackground>
            </View>
        );
    }
}
const styles = StyleSheet.create({

    MainContainer: {
        backgroundColor: '#FFF',
        margin: 10,
        paddingTop: (Platform.OS) === 'ios' ? 20 : 0
    },
    container: {
        flex: 1,
        flexDirection: 'row',
        marginBottom: 1,
        borderBottomColor: '#eee',
        height: '100%'
    },
    title: {
        justifyContent: 'flex-start',
        width: 120,
        textAlign: 'right',
        justifyContent: 'center',
        alignItems: 'center',
        padding: 10,
        backgroundColor: '#eee',
    },
    value: {
        fontSize: 18,
        fontWeight: '600',
        justifyContent: 'center',
        alignItems: 'center',
        padding: 9,
        width: null
    },

    GridViewBlockStyle: {

        justifyContent: 'center',
        flex: 1,
        alignItems: 'center',
        height: 100,
        margin: 1,
        backgroundColor: '#00BCD4'

    },
    GridViewImageStyle: {
        backgroundColor: '#ccc',
        flex: 1,
        resizeMode,
        position: 'absolute',
        width: '100%',
        height: '100%',
        justifyContent: 'center'
    }
    ,

    GridViewInsideTextItemStyle: {
        backgroundColor: 'rgba(0, 0, 0, 0.3)',
        color: '#fff',
        padding: 5,
        fontSize: 18,
        justifyContent: 'center',
        position: 'absolute',
        bottom: 0,
        width: '100%'

    },


    scrollContainer: {

    },
    image: {
        width,

    },

});
export default Appointment