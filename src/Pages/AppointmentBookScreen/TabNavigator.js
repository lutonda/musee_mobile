import React from 'react'


import {createStackNavigator, createBottomTabNavigator} from 'react-navigation'
import Calendar from './Calendar';

const CitiesNav = createStackNavigator({
    Calendário:{screen: Calendar},
    Visitas:{screen: Calendar},
    Eventos:{screen: Calendar}
},{
    navigationOptions:{
        headerStyle:{
            backgroundColor: '#F00'
        },
        headerTintColor:'#FFF'
    }
})

const TabNavigator = createBottomTabNavigator({
    Calendário:{screen: Calendar},
    Visitas:{screen: Calendar},
    Eventos:{screen: Calendar}
})

export default TabNavigator