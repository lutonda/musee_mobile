import React, { Component } from 'react';
import {
    View, StyleSheet, Dimensions, Image, Text,
    Alert, Platform,
    ImageBackground,
} from 'react-native'


import Icon from 'react-native-vector-icons/FontAwesome'
import { Header, Left, Right } from 'native-base'
import TabNavigator from 'react-native-tab-navigator';

import Calendario from './Calendar'
import TimeLine from './TimeLine'
import List from './List'

const resizeMode = 'center';
const { width } = Dimensions.get('window');
import Loader from '../../components/Loader'
import Service, { Services } from './../../Helpers';

class AppointmentBookScreen extends Component {

    constructor(props) {
        super(props);
        this.state = { index: 0, selectedTab: 'timeline', loading: true, eventos: [] }

        new Service({
            target: Services.getAppointments,
            successCallBack: this.successAppointment
        }).call()
    }

    successAppointment = (res) => {
        if (res != null)
            this.setState({ eventos: res })
        else
            Alert.alert('Erro');
        this.setState({ loading: false })
    }

    static navigationOptions = {
        title: 'Agenda',
        drawerIcon: (<Image source={require('./../../assets/icons/color/calendario.png')} style={{ width: 28, height: 28 }} />)
    }
    _handleMapRegionChange = mapRegion => {
        this.setState({ mapRegion });
    };
    render() {
        return (
            <View style={{ height: '100%' }}>
                <Loader loading={this.state.loading} />
                <Header style={{ backgroundColor: '#eee', flexDirection: 'row', flexWrap: 'wrap', alignItems: 'flex-start', textAlign: 'left', height: 40 }}>
                    <Left style={{ position: 'relative', left: 0, width: 100, alignSelf: 'flex-start' }}>
                        <Icon name='align-justify' style={{ padding: 5 }} size={25} onPress={() => this.props.navigation.openDrawer()} />
                    </Left>
                    <Right><Text style={{ fontSize: 24 }}></Text></Right>
                </Header>
                <ImageBackground style={{ flex: 1 }} source={require('../../img/bg/bg_2.jpg')}>
                    <TabNavigator style={{ height: '100%' }}
                        tabBarStyle={{ backgroundColor: '#FFF', height: 62 }}
                        sceneStyle={{ paddingBottom: 60 }}>
                        <TabNavigator.Item
                            selected={this.state.selectedTab === 'timeline'}
                            title="Linha de Tempo"
                            renderIcon={() => <Image source={require('./../../assets/icons/color/cardapio.png')} style={{ height: 30, width: 30, }} />}
                            onPress={() => this.setState({ selectedTab: 'timeline' })}>
                            <TimeLine />
                        </TabNavigator.Item>
                        <TabNavigator.Item
                            selected={this.state.selectedTab === 'calendar'}
                            title="Calendario"
                            renderIcon={() => <Image source={require('./../../assets/icons/color/calendario.png')} style={{ height: 30, width: 30, }} />}
                            onPress={() => this.setState({ selectedTab: 'calendar' })}>
                            <Calendario navigation={this.props.navigation} eventos={this.state.eventos} style={{ height: '100%', backgroundColor: '#0CF' }} />
                        </TabNavigator.Item>
                        <TabNavigator.Item
                            selected={this.state.selectedTab === 'lista'}
                            title="Lista"
                            renderIcon={() => <Image source={require('./../../assets/icons/color/lista-de-verificacao.png')} style={{ height: 30, width: 30, }} />}
                            onPress={() => this.setState({ selectedTab: 'lista' })}>
                            <List eventos={this.state.eventos} />
                        </TabNavigator.Item>
                        <TabNavigator.Item
                            selected={this.state.selectedTab === 'mine'}
                            title="Meus Eventos"
                            renderIcon={() => <Image source={require('./../../assets/icons/color/hoje.png')} style={{ height: 30, width: 30, }} />}
                            onPress={() => this.setState({ selectedTab: 'mine' })}>
                            <Text>Meus eventos</Text>
                        </TabNavigator.Item>
                    </TabNavigator>
                </ImageBackground>
            </View>
        );
    }
}
export default AppointmentBookScreen