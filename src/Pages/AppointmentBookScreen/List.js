import React, { Component } from 'react';
import {
    View,
    StyleSheet,
    Image,
    Text,
    ScrollView,
    ImageBackground,
    Dimensions,
    FlatList,
    TouchableOpacity
} from 'react-native'
import EventCalendar from 'react-native-events-calendar'
import Moment from 'react-moment';

import style from './style'
import { Icon } from 'native-base';

let { width } = Dimensions.get('window')

var Eventos = []
class List extends Component {
    constructor(props) {
        super(props)
        this.state = { eventos: [] }
        this.props.eventos.map((evento) => {
            evento.data = new Date(evento.dataAgenda.timestamp * 1000);
            Eventos.push(evento)
        })
        this.setState({ eventos: Eventos })
    }
    add_minutes(dt, minutes) {
        return new Date(dt + minutes * 60000);
    }
    add_hour(dt, hour) {
        return dt.setHours(dt.getHours() + hour);
    }
    add_time(dt, time) {
        if (time === null)
            return dt
        time = time.split(':')
        dt = this.add_hour(dt, time[0])
        dt = this.add_minutes(dt, time[1])
    }
    _eventTapped(event) {
        alert(JSON.stringify(event))
    }
    render() {
        return (
            <ScrollView>
                <View style={styles.MainContainer}>
                    <FlatList data={this.props.eventos} renderItem={(evento) =>
                        <View style={styles.container} onPress={() => this.GetGridViewItem(evento)}>
                            <ImageBackground style={styles.logo} source={require('../../img/wp/walpaper_e.jpg')}>
                                <View style={{ backgroundColor: 'rgba(0,0,0,.2)', padding: 10, bottom: 0, position: 'absolute',width:'100%' }}>
                                    <Text style={styles.title}>{evento.item.idevento.titulo}</Text>
                                </View>
                            </ImageBackground>
                            <View style={{ flex: 1, flexDirection: 'row', borderBottomColor: '#333', borderBottomWidth: 0.2,backgroundColor:'#333' }}>
                                <View style={{ flex: 1 }}>
                                    <Text style={styles.footer}>por: {evento.item.orador.nome}</Text>
                                </View>
                                <View style={{ flex: 1 }}>
                                    <Text style={styles.footer}>{evento.item.idevento.tipo.nome}</Text>
                                </View>
                                <View style={{ flex: 1 }}>
                                    <Text style={styles.footer}>{evento.item.idlocal.nome} - {evento.item.idlocal.descricao}</Text>
                                </View>
                            </View>
                            <View>
                                <Text style={styles.paragraph}>{evento.item.idevento.descricao}</Text>
                            </View>
                            <View style={{ flexDirection: 'row', flex: 1 }}>
                                <View style={{ flex: 2 }}>
                                    <Text style={styles.footer}>{evento.item.dataAgenda.timestamp} [{evento.item.dataAgenda.horainicial} - {evento.item.horafinal}]</Text>
                                </View>
                                <View style={{ flex: 1 }}>
                                    <TouchableOpacity style={styles.botton} onPress={() => this.GetGridViewItem(evento)}>
                                        <Text style={{ color: '#FFF', textAlign: 'center', width: '100%' }}>Inscrever-se</Text>
                                    </TouchableOpacity>
                                </View>

                            </View>
                        </View>
                    }
                    />
                </View>
            </ScrollView>
        )
    }
}

const styles = StyleSheet.create({

    MainContainer: {
        justifyContent: 'center',
        flex: 1,
        height: '100%'
    },
    container: {
        padding: 20,
        minHeight: 200,
        margin: 10,
        marginBottom: 0,
        flexDirection: "column",
        borderRadius: 3,
        backgroundColor: '#FFF',
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.8,
        shadowRadius: 2,
        elevation: 1,
    },

    paragraph: {
        textAlign: 'left',
    },
    title: {
        fontSize: 24,
        padding: 3
    },
    body: {
        color: '#345'
    },
    footer: {
        textAlign: 'left',
        color:'#fff'
    },
    logo: {
        backgroundColor: '#f0f0f0',
        height: 180,
    },
    botton: {
        backgroundColor: '#409fbf',
        padding: 10,
        borderRadius: 3,
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.8,
        shadowRadius: 2,
        elevation: 1,

    }
});
export default List