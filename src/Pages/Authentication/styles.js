import {StyleSheet} from 'react-native'

const styles = StyleSheet.create({
  controlContainer: {
    marginTop: 20,
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  signIncontainer: {
    flex: 1,
  },
  signUpcontainer: {
    flex: 1,
    height: '100%',
  },
  formContainer: {
    flex: 1,
  },
  wraper: {
    flex: 1,
  },
  wallpaper: {
    flex: 1,
    width: null,
    height: null,
    resizeMode: 'cover',
    padding: 20,
  },
  container: {
    flex: 1,
  },
  logo: {
    width: 90,
    height: 90
  },
  logoContainer: {
    alignItems: 'center',
    flex: 1,
    flexGrow: 1,
    justifyContent: 'center',
    padding: 20,
  },
  title: {
    fontSize: 26,
    color: '#4fb9bf',
    marginTop: 10,
    width: 200,
    textAlign: 'center',

  },
  text: { color: '#4fb9bf' },
  textInput: {
    alignSelf: 'stretch',
    marginBottom: 20,
    paddingHorizontal: 10,
    color: '#8290a3',
    backgroundColor: '#dde3ec',
  },
  btn: {
    alignSelf: 'stretch',
    backgroundColor: '#034d96',
    padding: 20,
    alignItems: 'center'

  }

})
export default styles