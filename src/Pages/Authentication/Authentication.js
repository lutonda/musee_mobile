import React, { Component } from 'react';
import {
  Text
  , TextInput
  , ImageBackground
  , View
  , Image
  , AsyncStorage
  , Alert
  , KeyboardAvoidingView
} from 'react-native';
import { Button } from 'react-native-elements'
import { Actions } from 'react-native-router-flux';
import SlidingUpPanel from 'rn-sliding-up-panel';
import background from '../../img/bg/bg_4.jpg';
import Loader from '../../components/Loader'

import Service, { Services } from './../../Helpers'

import styles from './styles'
class Authentication extends Component {

  constructor(props) {
    super(props);
    this.state = {
      loading: true
      , loginValid: true
      , registerValid: true
      , visible: false
      , logOnData: {}
      , logInData: {}
      , signedUp: false,
    };
  }

  componentDidMount() {
    this.setState({ loading: false })
  }

  userSignIn() {
    new Service({
      target: Services.logIn,
      data: this.state.logInData,
      successCallBack: this.successLogInCallBack
    }).call()
  }

  userSignUp() {
    new Service({
      target: Services.logOn,
      data: this.state.logOnData,
      successCallBack: this.successLogOnCallBack
    }).call()
  }
  successLogInCallBack = (res) => {
    if (res.username) {
      AsyncStorage.setItem('userData', JSON.stringify(res))
      Actions.HomePage()
    }
    else {
      Alert.alert('Erro', 'Dados não validos, verifique o seu nome de utilizador e/ou a sua palavrapasse e volte a tentar')
      this.setState({ username: '', password: '' })
      return false
    }
  }
  successLogOnCallBack = (res) => {
    if (res[0]) {
      AsyncStorage.setItem('userData', JSON.stringify(res[1]))
      Actions.HomePage()
    }
    else {
      var msg = 'Erro ao Registar1\n'
      for (var i = 0; i < res[1].length; i++)
        msg += (i + 1) + '- ' + res[1][i] + '\n'
      alert(msg)
      return false
    }
  }

  onChangeText = (key, value) => {
    if (key.indexOf(".") > 0) {
      key = key.split('.')
      var obj = this.state[key[0]]
      obj[key[1]] = value
      value = obj
      key = key[0]
    }
    this.setState({ [key]: value })
    this.setState({
      loginValid:
        (this.state.logInData.username === '' ||
          this.state.logInData.password === '')
    })
    this.setState({
      registerValid:
        (this.state.logOnData.username === '' ||
          this.state.logOnData.password === '' ||
          this.state.logOnData.email === '' ||
          this.state.logOnData.nomecompleto === ''

        )
    })
  }

  render() {
    return (
      <View style={styles.container}>
        <Loader loading={this.state.loading} />
        <ImageBackground style={styles.wallpaper} source={background}>
          <View style={styles.formContainer}>
          <SlidingUpPanel
                visible={!this.state.visible}
                onRequestClose={() => this.setState({ visible: false })} style={{ flex: 1, backgroundColor: '#F00' }}>
                <KeyboardAvoidingView behavior="padding" style={{ padding: 10, flex: 1, backgroundColor: '#FFF' }}>
                    <View style={[styles.logoContainer]}>
                        <Image style={styles.logo} source={require('../../img/logo/logo_mini_dark.png')} />
                        <Text style={styles.title}>Museu de Geologia</Text>
                    </View>
                    <View style={{ flex: 2, justifyContent: 'center', }}>
                        <TextInput
                            style={styles.textInput}
                            placeholder='Username'
                            placeholderTextColor='rgba(0,0,0,0.5)'
                            value={this.state.logInData.username}
                            onChangeText={val => this.onChangeText('logInData.username', val)}
                            underlineColorAndroid='transparent' />
                        <TextInput
                            style={styles.textInput}
                            placeholder='Password'
                            placeholderTextColor='rgba(0,0,0,0.5)'
                            value={this.state.logInData.password}
                            onChangeText={val => this.onChangeText('logInData.password', val)}
                            underlineColorAndroid='transparent'
                            secureTextEntry
                            password='true' />
                        <Button onPress={this.userSignIn.bind(this)} title='Entrar' disabled={this.state.loginValid} />
                        <View style={styles.controlContainer}>
                            <Text style={styles.text} onPress={() => this.setState({ visible: !this.state.visible })} >Registar-se</Text>
                            <Text style={styles.text}>Preciso de Ajuda</Text>
                        </View>
                    </View>
                </KeyboardAvoidingView>
            </SlidingUpPanel>
            <SlidingUpPanel
              visible={this.state.visible}
              onRequestClose={() => this.setState({ visible: false })} >
              <KeyboardAvoidingView behavior="padding" style={{ flex: 1, padding:10, justifyContent: 'center', backgroundColor: '#FFF' }}>
                <TextInput
                  style={styles.textInput}
                  placeholder='Nome Completo'
                  placeholderTextColor='rgba(0,0,0,0.5)'
                  value={this.state.logOnData.nomecompleto}
                  onChangeText={val => this.onChangeText('logOnData.nomecompleto', val)}
                  underlineColorAndroid='transparent' />
                <TextInput
                  style={styles.textInput}
                  placeholder='Username'
                  placeholderTextColor='rgba(0,0,0,0.5)'
                  value={this.state.logOnData.username}
                  onChangeText={val => this.onChangeText('logOnData.username', val)}
                  underlineColorAndroid='transparent' />
                <TextInput
                  style={styles.textInput}
                  placeholder='E-mail'
                  placeholderTextColor='rgba(0,0,0,0.5)'
                  value={this.state.logOnData.email}
                  onChangeText={val => this.onChangeText('logOnData.email', val)}
                  underlineColorAndroid='transparent' />
                <TextInput
                  style={styles.textInput}
                  placeholder='Palavra Passe'
                  placeholderTextColor='rgba(0,0,0,0.5)'
                  value={this.state.logOnData.password}
                  onChangeText={val => this.onChangeText('logOnData.password', val)}
                  underlineColorAndroid='transparent'
                  secureTextEntry
                  password='true' />
                <Button onPress={this.userSignUp.bind(this)} title='Registar' disabled={this.state.RegisterValid} />

                <View style={styles.controlContainer}>
                  <Text style={styles.text} onPress={() => this.setState({ visible: !this.state.visible })} >Ja Tenho um conta? Autentica-se</Text>
                </View>
              </KeyboardAvoidingView>
            </SlidingUpPanel>
          </View>
        </ImageBackground>
      </View>
    );
  }
}

export default Authentication;