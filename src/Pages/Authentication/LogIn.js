import React, { Component } from 'react'
export default class LogInScreen extends Component {
    constructor(props){
        super(props)
    }
    render() {
        return (
            <SlidingUpPanel
                visible={!this.state.visible}
                onRequestClose={() => this.setState({ visible: false })} style={{ flex: 1, backgroundColor: '#F00' }}>
                <KeyboardAvoidingView behavior="padding" style={{ padding: 10, flex: 1, backgroundColor: '#FFF' }}>
                    <View style={[styles.logoContainer]}>
                        <Image style={styles.logo} source={require('../../img/logo/logo_mini_dark.png')} />
                        <Text style={styles.title}>Museu de Geologia</Text>
                    </View>
                    <View style={{ flex: 2, justifyContent: 'center', }}>
                        <TextInput
                            style={styles.textInput}
                            placeholder='Username'
                            placeholderTextColor='rgba(0,0,0,0.5)'
                            value={this.state.logInData.username}
                            onChangeText={val => this.onChangeText('logInData.username', val)}
                            underlineColorAndroid='transparent' />
                        <TextInput
                            style={styles.textInput}
                            placeholder='Password'
                            placeholderTextColor='rgba(0,0,0,0.5)'
                            value={this.state.logInData.password}
                            onChangeText={val => this.onChangeText('logInData.password', val)}
                            underlineColorAndroid='transparent'
                            secureTextEntry
                            password='true' />
                        <Button onPress={this.userSignIn.bind(this)} title='Entrar' disabled={this.state.loginValid} />
                        <View style={styles.controlContainer}>
                            <Text style={styles.text} onPress={() => this.setState({ visible: !this.state.visible })} >Registar-se</Text>
                            <Text style={styles.text}>Preciso de Ajuda</Text>
                        </View>
                    </View>
                </KeyboardAvoidingView>
            </SlidingUpPanel>
        )
    }
}