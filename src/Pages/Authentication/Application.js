import React, { Component } from 'react';
class Application extends Component {

    constructor (data) {
        if (typeof data === 'undefined') {
            throw new Error('Cannot be called directly');
        }
    }

    async saveItem(item, selectedValue) {
        try {
            await AsyncStorage.setItem(item, selectedValue);
            return true
        } catch (error) {
            return error.message
        }
    }

    static  userSignup() {
    }

}
export default Application