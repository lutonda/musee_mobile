
import React, { Component } from 'react';
import {
    Image,
    View,
    Text,
    StyleSheet,
    ScrollView,
    FlatList,
    AsyncStorage,
    ImageBackground,
    TouchableOpacity
} from 'react-native'

import { Container, Content, } from 'native-base'
import Loader from '../../components/Loader'
export default class Thumbnail extends React.Component {

    constructor(props) {
        super(props);
        this.state = { loading: true, GridViewItems: [] }

        fetch('http://192.168.100.3:8000/api/Rest/todosacervos', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'content-Type': 'application/json',
                'apikey': 'newapikeyforuseb',
                'X-AUTH-TOKEN': 123,

            },
            body: JSON.stringify({ categoria: 'mineralogia' })
        })
            .then((response) => response.json())
            .then((res) => {
                if (res != null) {
                    this.setState({ GridViewItems: res })
                }
                else {
                    Alert.alert('Erro');
                }
            })
            .catch((error) => {
                Alert.alert('Erro');
            }).done(() => {
                this.setState({ loading: false })
            })
    }

    GetGridViewItem(item) {
        this.props.navigation.navigate('Item', { item: item })
    }

    render() {
        const { navigate } = this.props.navigation;
        return (
            <ImageBackground style={{height:'100%'}} source={require('../../img/bg/bg_3.jpg')}>
                <Loader loading={this.state.loading} />
                <FlatList
                    numColumns={2}
                    data={this.state.GridViewItems}
                    renderItem={({ item }) =>
                        <TouchableOpacity style={styles.container} onPress={() => this.GetGridViewItem(item)} >
                            <ImageBackground style={styles.GridViewImageStyle} source={{ uri: item.anexos[0].data64 }}>
                                <Text style={styles.GridViewInsideTextItemStyle}>
                                    {item.nome}
                                </Text>
                            </ImageBackground>
                        </TouchableOpacity>}
                />
            </ImageBackground>
        )
    }
}

const styles = StyleSheet.create({

    GridViewImageStyle: {
        backgroundColor: '#ccc',
        flex: 1,
        position: 'absolute',
        width: '100%',
        height: '100%',
        justifyContent: 'center'
    },

    GridViewInsideTextItemStyle: {
        backgroundColor: 'rgba(0, 0, 0, 0.3)',
        color: '#fff',
        padding: 5,
        fontSize: 18,
        justifyContent: 'center',
        position: 'absolute',
        bottom: 0,
        width: '100%'

    },

    container: {

        justifyContent: 'center',
        flex: 1,
        alignItems: 'center',
        height: 100,
        margin: 5,
        marginBottom: 0,
        borderWidth: 1,
        borderRadius: 0,
        backgroundColor: '#FFF',
        borderColor: '#FFF',
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.8,
        shadowRadius: 2,
        elevation: 1,
    },
});