import React, { Component } from 'react';
import {
    View, StyleSheet, Dimensions, Image, Text,
    ScrollView,
    TouchableWithoutFeedback,
    AppRegistry, FlatList, Alert, Platform,
    AsyncStorage,
    ImageBackground,MapView
} from 'react-native'


import Icon from 'react-native-vector-icons/FontAwesome'
import { Button, Container, Content, Header, Left, Right } from 'native-base'
import { TabView, TabBar, SceneMap } from 'react-native-tab-view';
import TabNavigator from 'react-native-tab-navigator';

import Gerais from './Item/Gerais'
import Fisicas from './Item/Fisicas'
import Opticas from './Item/Opticas'
import Quimicas from './Item/Quimicas'
import Cristalograficas from './Item/Cristalograficas'
import Maps from './Item/Maps';

const resizeMode = 'center';
const { width } = Dimensions.get('window');
const height = width * 0.5;

import Loader from '../Loader'
class Item extends Component {

    constructor(props) {
        super(props);
        this.state = { index: 0,selectedTab: 'home', loading: false, GridViewItems: [], item: this.props.navigation.state.params.item }
    }
    _handleMapRegionChange = mapRegion => {
        this.setState({ mapRegion });
    };
    render() {
        const resizeMode = 'center';
        return (

            <View style={{ height: '100%' }}>
                <Loader loading={this.state.loading} />
                <Header style={{ backgroundColor: '#eee', flexDirection: 'row', flexWrap: 'wrap', alignItems: 'flex-start', textAlign: 'left', height: 40 }}>
                    <Left style={{ position: 'relative', left: 0, width: 100, alignSelf: 'flex-start' }}>
                        <Icon name='align-justify' style={{ padding: 5 }} size={25} onPress={() => this.props.navigation.openDrawer()} />
                    </Left>
                    <Right><Text style={{ fontSize: 24 }}>{this.state.item.nome}</Text></Right>
                </Header>

                <View style={{ height: 200 }}>
                    <ScrollView style={styles.scrollContainer}
                        horizontal
                        pagingEnabled
                        showsHorizontalScrollIndicator={true}>
                        {this.state.item.anexos.map(image => (
                            <Image style={styles.image} source={{ uri: image.data64 }} />
                        ))}
                    </ScrollView>
                </View>

                <ImageBackground style={{ flex: 1 }} source={require('../../img/bg/bg_1.jpg')}>
                    <TabNavigator style={{ height: '100%' }}
                        tabBarStyle={{ backgroundColor: '#FFF', height: 62 }}
                        sceneStyle={{ paddingBottom: 60 }}>
                        <TabNavigator.Item
                            selected={this.state.selectedTab === 'home'}
                            title="Gerais"
                            renderIcon={() => <Image source={require('../../img/icons/007-computer-1.png')} style={{ height: 30, width: 30, }} />}
                            onPress={() => this.setState({ selectedTab: 'home' })}>
                            <Gerais Item={this.state.item} />
                        </TabNavigator.Item>
                        <TabNavigator.Item
                            selected={this.state.selectedTab === 'fisicos'}
                            title="Físicos"
                            renderIcon={() => <Image source={require('../../img/icons/006-puzzle.png')} style={{ height: 30, width: 30, }} />}
                            onPress={() => this.setState({ selectedTab: 'fisicos' })}>
                            <Fisicas Item={this.state.item} />
                        </TabNavigator.Item>
                        <TabNavigator.Item
                            selected={this.state.selectedTab === 'quimicos'}
                            title="Quimicos"
                            renderIcon={() => <Image source={require('../../img/icons/018-clipboard-1.png')} style={{ height: 30, width: 30, }} />}
                            onPress={() => this.setState({ selectedTab: 'quimicos' })}>
                            <Quimicas Item={this.state.item} />
                        </TabNavigator.Item>
                        <TabNavigator.Item
                            selected={this.state.selectedTab === 'optico'}
                            title="Ópticos"
                            renderIcon={() => <Image source={require('../../img/icons/039-search.png')} style={{ height: 30, width: 30, }} />}
                            onPress={() => this.setState({ selectedTab: 'optico' })}>
                            <Opticas Item={this.state.item} />
                        </TabNavigator.Item>
                        <TabNavigator.Item
                            selected={this.state.selectedTab === 'cristalograficos'}
                            title="Cristal"
                            renderIcon={() => <Image source={require('../../img/icons/049-networking.png')} style={{ height: 30, width: 30, }} />}
                            onPress={() => this.setState({ selectedTab: 'cristalograficos' })}>
                            <Cristalograficas Item={this.state.item} />
                        </TabNavigator.Item>
                        <TabNavigator.Item
                            selected={this.state.selectedTab === 'maps'}
                            title="Localização"
                            renderIcon={() => <Image source={require('../../img/icons/025-compass.png')} style={{ height: 30, width: 30, }} />}
                            onPress={() => this.setState({ selectedTab: 'maps' })}>
                            <Maps/>
                        </TabNavigator.Item>
                    </TabNavigator>
                </ImageBackground>
            </View>
        );
    }
}
const styles = StyleSheet.create({

    MainContainer: {
        backgroundColor: '#FFF',
        margin: 10,
        paddingTop: (Platform.OS) === 'ios' ? 20 : 0
    },
    container: {
        flex: 1,
        flexDirection: 'row',
        marginBottom: 1,
        borderBottomColor: '#eee',
        height: '100%'
    },
    title: {
        justifyContent: 'flex-start',
        width: 120,
        textAlign: 'right',
        justifyContent: 'center',
        alignItems: 'center',
        padding: 10,
        backgroundColor: '#eee',
    },
    value: {
        fontSize: 18,
        fontWeight: '600',
        justifyContent: 'center',
        alignItems: 'center',
        padding: 9,
        width: null
    },

    GridViewBlockStyle: {

        justifyContent: 'center',
        flex: 1,
        alignItems: 'center',
        height: 100,
        margin: 1,
        backgroundColor: '#00BCD4'

    },
    GridViewImageStyle: {
        backgroundColor: '#ccc',
        flex: 1,
        resizeMode,
        position: 'absolute',
        width: '100%',
        height: '100%',
        justifyContent: 'center'
    }
    ,

    GridViewInsideTextItemStyle: {
        backgroundColor: 'rgba(0, 0, 0, 0.3)',
        color: '#fff',
        padding: 5,
        fontSize: 18,
        justifyContent: 'center',
        position: 'absolute',
        bottom: 0,
        width: '100%'

    },


    scrollContainer: {

    },
    image: {
        width,

    },

});
export default Item