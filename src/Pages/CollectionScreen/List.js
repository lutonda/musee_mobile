import React, { Component } from 'react';
import {
    Image,
    View,
    Text,
    StyleSheet,
    ScrollView,
    FlatList,
    Platform,
    AsyncStorage,
    ImageBackground,
    TouchableOpacity
} from 'react-native'

import { Container, Content, } from 'native-base'
import Icon from 'react-native-vector-icons/FontAwesome'

import Loader from '../../components/Loader'

export default class List extends React.Component {

    constructor(props) {
        super(props);
        this.state = { loading: true, GridViewItems: [] }

        fetch('http://192.168.100.3:8000/api/Rest/todosacervos', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'content-Type': 'application/json',
                'apikey': 'newapikeyforuseb',
                'X-AUTH-TOKEN': 123,

            },
            body: JSON.stringify({ categoria: 'mineralogia' })
        })
            .then((response) => response.json())
            .then((res) => {
                if (res != null) {
                    this.setState({ GridViewItems: res })
                }
                else {
                    Alert.alert('Erro');
                }
            })
            .catch((error) => {
                Alert.alert('Erro');
            }).done(() => {
                this.setState({ loading: false })
            })
    }

    GetGridViewItem(item) {
        this.props.navigation.navigate('Item', { item: item })
    }
    render() {
        return (
            <ImageBackground style={{ height: '100%' }} source={require('../../img/bg/bg_3.jpg')}>
                <Loader loading={this.state.loading} />
                <FlatList data={this.state.GridViewItems} renderItem={({ item }) =>
                    <TouchableOpacity style={styles.container} onPress={() => this.GetGridViewItem(item)}>
                        <ImageBackground style={styles.logo} source={{ uri: item.anexos[0].data64 }} />
                        <View style={styles.paragraph}>
                            <Text style={styles.header}>{item.nome}</Text>
                            <Text style={styles.body}>{item.importacia} - {item.caraceristica}</Text>
                            <Text style={styles.footer}>{item.idcategoria.nome}</Text>
                        </View>
                    </TouchableOpacity>
                }
                />
            </ImageBackground>
        )
    }
}

const styles = StyleSheet.create({

    MainContainer: {
        justifyContent: 'center',
        flex: 1,
        paddingTop: (Platform.OS) === 'ios' ? 20 : 0
    },
    container: {
        margin: 5,
        marginBottom: 0,
        flexDirection: "row",
        borderWidth: 5,
        borderRadius: 0,
        backgroundColor: '#FFF',
        borderColor: '#FFF',
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.8,
        shadowRadius: 2,
        elevation: 1,
    },

    paragraph: {
        padding: 12,
        paddingRight: 50,
        textAlign: 'left',
    },
    header: {
        fontSize: 22
    },
    body: {
        color: '#345'
    },
    footer: {
        textAlign: 'left'
    },
    logo: {
        backgroundColor: '#056ecf',
        height: 90,
        width: 110,
    },
});