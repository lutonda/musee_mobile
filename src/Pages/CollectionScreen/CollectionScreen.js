import React, {PureComponent} from 'react';
import {
    StyleSheet,Image
} from 'react-native'

import { TabViewAnimated, TabBar, SceneMap } from 'react-native-tab-view'; // 0.0.69
import Icon from 'react-native-vector-icons/FontAwesome'
import { Container, Header, Left, Right} from 'native-base'


import Collection from './TabNavigator';
const FirstRoute = () => (<Thumbnail navigation={this.props.navigation}/>);
const SecondRoute = () => (<List onPress={this.props} />);

class CollectionScreen extends PureComponent {
    constructor(props) {
        super(props);
        
    }
    static navigationOptions = {
        title: 'Acervo',
        drawerIcon: (<Image source={require('./../../assets/icons/color/caixa-de-ferramentas.png')} style={{width:28,height:28}}/>)
    }
    render() {
        return (
            <Container>
                <Header style={{
                    backgroundColor: '#eee',
                    flexDirection: 'row',
                    flexWrap: 'wrap',
                    alignItems: 'flex-start',
                    textAlign: 'left',
                    height: 40
                }}>
                    <Left style={{position: 'relative', left: 0, width: 100, alignSelf: 'flex-start'}}>
                        <Icon name='align-justify' style={{padding: 5}} size={25}
                              onPress={() => this.props.navigation.openDrawer()}/>
                    </Left>
                    <Right>

                    </Right>
                </Header>
                <Collection  {...this.props}/>
            </Container>
        )
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
    },
  });
export default CollectionScreen