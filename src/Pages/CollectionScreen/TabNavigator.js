import React, { PureComponent } from 'react';
import { View, StyleSheet, Icon,ImageBackground } from 'react-native';
import { TabViewAnimated, TabBar, SceneMap } from 'react-native-tab-view'; // 0.0.69

import Thumbnail from './Thumbnail'
import List from './List'

class TabNavigator extends PureComponent {
  
  state = {
    index: 0,
    routes: [
      { key: '1', title: 'Miniaturas', icon: 'home', tabStyle: { backgroundColor: 'red' } },
      { key: '2', title: 'Listas', icon: 'home' }],
  };

  GetGridViewItem (item) {
    this.props.navigation.navigate('Item', {item:item})
    }
  _handleIndexChange = index => this.setState({ index });
  _renderHeader = props => <TabBar {...props} />;
  _renderScene = SceneMap({
    '1': () => (<Thumbnail  navigation={this.props.navigation}/>),
    '2':  () => (<List  navigation={this.props.navigation}/>),
  });

  render() {
    return (
      <TabViewAnimated
        style={styles.container}
        navigationState={this.state}
        renderScene={this._renderScene}
        renderHeader={this._renderHeader}
        onIndexChange={this._handleIndexChange}
        navigation={this.props.navigation}
      />
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});

export default TabNavigator