import React, { Component } from 'react';
import{
    Image,
View,
Text,
StyleSheet,
ScrollView,
TouchableWithoutFeedback,
AppRegistry, FlatList, Alert, Platform,
AsyncStorage,
ImageBackground
} from 'react-native'

import Icon from 'react-native-vector-icons/FontAwesome'

import {DrawerNavigator, DrawerItems} from 'react-navigation'
import {Button,Container,Content,Header,Left,Right} from 'native-base'
import Loader from '../Loader'
const resizeMode = 'center';
class _CollectionScreen extends Component{

    constructor(props)
     {
       super(props);
     
       this.state = { loading: true,GridViewItems: []}
       
    debugger
        fetch('http://192.168.100.3:8000/api/Rest/todosacervos',{
          method:'POST',
          headers:{
            'Accept':'application/json',
            'content-Type':'application/json',
            'apikey': 'newapikeyforuseb',
            'X-AUTH-TOKEN': 123,
            
          },
          body:JSON.stringify({categoria:'mineralogia'})
        })
        .then((response)=>response.json())
        .then((res) =>{
          debugger
          if(res!=null){
              this.setState({GridViewItems:res})
          }
          else{
            Alert.alert('Erro');
          }
        })
        .catch((error) => {
          Alert.alert('Erro');
        }).done(()=>{
            this.setState({loading: false})
        })
      }
    static navigationOptions={
        title:'Acervo',
        drawerIcon:(<Icon name='archive' size={25}/>)
    }
    viewItem=(item)=>{
        this.props.navigation.navigate('Item', {item:item})
        }
    GetGridViewItem (item) {
        this.props.navigation.navigate('Item', {item:item})
        
        }

    
render(){
    const resizeMode = 'center';
    return(
        <Container>
        <Loader loading={this.state.loading} />
            <Header style={{backgroundColor:'#eee',flexDirection: 'row',flexWrap: 'wrap',alignItems:'flex-start',textAlign:'left', height:40}}>
                <Left style={{position: 'relative', left: 0,width: 100,alignSelf:'flex-start' }}>
                <Icon name='align-justify' style={{padding:5}} size={25} onPress={()=>this.props.navigation.openDrawer()}/>
                    </Left>
                    <Right><Text>Acervo</Text></Right>
                </Header>
            <Content>
                
                <ScrollView>
                <View style={styles.MainContainer}>
                        <FlatList
                            numColumns={2}
                            data={ this.state.GridViewItems }
                            renderItem={({item}) =>
                            <View style={styles.GridViewBlockStyle} >
                           <ImageBackground style={styles.GridViewImageStyle} source={{uri: item.anexos[0].data64}}>     
                                <Text style={styles.GridViewInsideTextItemStyle}
                                onPress={this.GetGridViewItem.bind(this, item)} > {item.nome} </Text>
                                </ImageBackground>
                                </View>} 
                            />
                </View>
                </ScrollView>
            </Content>
            </Container>
    )
}
}

const styles=StyleSheet.create({
    cityContainer:{
        padding:10,
        borderBottomWidth:2,
        borderBottomColor:'#F00'
    },
    city:{

    },
    country:{

    },

 
MainContainer :{
 
justifyContent: 'center',
flex:1,
margin: 10,
paddingTop: (Platform.OS) === 'ios' ? 20 : 0
 
},
 
GridViewBlockStyle: {
 
  justifyContent: 'center',
  flex:1,
  alignItems: 'center',
  height: 100,
  margin: 1,
  backgroundColor: '#00BCD4'
 
},
GridViewImageStyle:{
    backgroundColor: '#ccc',
          flex: 1,
          resizeMode,
          position: 'absolute',
          width: '100%',
          height: '100%',
          justifyContent: 'center'
}
,
 
GridViewInsideTextItemStyle: {
 backgroundColor:'rgba(0, 0, 0, 0.3)',
   color: '#fff',
   padding: 5,
   fontSize: 18,
   justifyContent: 'center',
   position: 'absolute',
      bottom: 0,
      width: '100%'
   
 },
 
});
export default _CollectionScreen