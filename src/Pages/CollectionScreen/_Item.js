import React, { Component } from 'react';
import {
    Image,
    View,
    Text,
    StyleSheet,
    ScrollView,
    TouchableWithoutFeedback,
    AppRegistry, FlatList, Alert, Platform,
    AsyncStorage,
    ImageBackground,
    Dimensions
} from 'react-native'

import Icon from 'react-native-vector-icons/FontAwesome'
import { Button, Container, Content, Header, Left, Right } from 'native-base'
import Loader from '../Loader'
import MapsView from '../Maps/MapsView'
const resizeMode = 'center';
const { width } = Dimensions.get('window');
const height = width * 0.5;

class Item extends Component {

    constructor(props) {
        super(props);
        this.state = { loading: false, GridViewItems: [], item: this.props.navigation.state.params.item }

        this.setState({ loading: false })
    }

    render() {
        const resizeMode = 'center';
        return (
            <Container>

                <Loader loading={this.state.loading} />
                <Header style={{ backgroundColor: '#eee', flexDirection: 'row', flexWrap: 'wrap', alignItems: 'flex-start', textAlign: 'left', height: 40 }}>
                    <Left style={{ position: 'relative', left: 0, width: 100, alignSelf: 'flex-start' }}>
                        <Icon name='align-justify' style={{ padding: 5 }} size={25} onPress={() => this.props.navigation.openDrawer()} />
                    </Left>
                    <Right><Text style={{ fontSize: 24 }}>{this.state.item.nome}</Text></Right>
                </Header>
                <Content>
                    <View style={styles.scrollContainer}>
                        <ScrollView
                            horizontal
                            pagingEnabled
                            showsHorizontalScrollIndicator={true}>
                            {this.state.item.anexos.map(image => (
                                <Image style={styles.image} source={{ uri: image.data64 }} />
                            ))}
                        </ScrollView>
                    </View>
                    <ScrollView style={styles.MainContainer}>
                        <View style={styles.container}>
                            <Text style={styles.title}>Categoria</Text>
                            <Text style={styles.value}>{this.state.item.idcategoria.nome}>{this.state.item.idcategoria.dependencia.nome}</Text>
                        </View>
                        <View style={styles.container}>
                            <Text style={styles.title}>Importância</Text>
                            <Text style={styles.value}>{this.state.item.importacia}</Text>
                        </View>
                        <View style={styles.container}>
                            <Text style={styles.title}>Caracteristica</Text>
                            <Text style={styles.value}>{this.state.item.caraceristica}</Text>
                        </View>
                        <View style={styles.container}>
                            <Text style={styles.title}>Conservação</Text>
                            <Text style={styles.value}>
                                -{this.state.item.idconservacao.temperaturaminima}
                                {this.state.item.idconservacao.temperaturamaxima}
                                -{this.state.item.idconservacao.sensibilidadeluminosa}</Text>
                        </View>
                        <View style={styles.container}>
                            <Text style={styles.title}>Hábito</Text>
                            <Text style={styles.value}>{this.state.item.idpropriedadefisica.idhabito.nome}-{this.state.item.idpropriedadefisica.idhabito.descricao}</Text>
                        </View>
                        <View style={styles.container}><Text> ... </Text></View>
                        <View style={styles.container}>
                            <Text style={styles.title}>Hábito</Text>
                            <Text style={styles.value}>{this.state.item.idprateleira.codigo}>{this.state.item.idprateleira.idseccao.codigo}</Text>
                        </View>
                        <View>
                            <Text>-</Text>
                            <MapsView />
                        </View>
                    </ScrollView>
                </Content>
            </Container>
        )
    }
}
const styles = StyleSheet.create({

    MainContainer: {
        backgroundColor: '#FFF',
        margin: 10,
        paddingTop: (Platform.OS) === 'ios' ? 20 : 0
    },
    container: {
        flex: 1,
        flexDirection: 'row',
        marginBottom: 1,
        borderBottomColor: '#eee',
    },
    title: {
        justifyContent: 'flex-start',
        width: 120,
        textAlign: 'right',
        justifyContent: 'center',
        alignItems: 'center',
        padding: 10,
        backgroundColor: '#eee',
    },
    value: {
        fontSize: 18,
        fontWeight: '600',
        justifyContent: 'center',
        alignItems: 'center',
        padding: 9,
        width: null
    },

    GridViewBlockStyle: {

        justifyContent: 'center',
        flex: 1,
        alignItems: 'center',
        height: 100,
        margin: 1,
        backgroundColor: '#00BCD4'

    },
    GridViewImageStyle: {
        backgroundColor: '#ccc',
        flex: 1,
        resizeMode,
        position: 'absolute',
        width: '100%',
        height: '100%',
        justifyContent: 'center'
    }
    ,

    GridViewInsideTextItemStyle: {
        backgroundColor: 'rgba(0, 0, 0, 0.3)',
        color: '#fff',
        padding: 5,
        fontSize: 18,
        justifyContent: 'center',
        position: 'absolute',
        bottom: 0,
        width: '100%'

    },


    scrollContainer: {
        height,
    },
    image: {
        width,
        height,
    },

});
export default Item