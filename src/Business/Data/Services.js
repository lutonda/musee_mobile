export default  Services = {
    logIn: {
        method: 'POST',
        url: 'mus/verificar/utilizador'
    },
    logOn: {
        method: 'POST',
        url: 'mus/registrar/utilizador'
    },
    getAppointments: {
        method: 'POST',
        url: 'mev/eventos/proximasagendas'
    },
    getAppointment: {
        method: 'POST',
        url: 'mev/eventos/proximasagendas'
    },

}