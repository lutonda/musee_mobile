export default class Service {
    headers = {
        'Accept': 'application/json',
        'content-Type': 'application/json',
        'apikey': 'newapikeyforuseb',
        'X-AUTH-TOKEN': 123,
    };
    /**
     * target -> String,
     * successCallBack -> fn
     * data -> JSON
     * method -> POST,GET,DELET,PUT
     */
    constructor(obj) {
        this.target = obj.target;
        this.successCallBack = (obj.successCallBack || this.defaultfn);
        this.method = this.target.method;
        this.data = obj.data;
        return this
    };

    defaultfn = (e) => {
        console.log(JSON.stringify(e))
    }
    call = () => {
        try {
            fetch('http://192.168.100.5:8000/api/Rest/' + this.target.url, {
                method: this.target.method,
                headers: this.headers,
                body: JSON.stringify(this.data)
            })
                .then((response) => response.json())
                .then(this.successCallBack)
                .catch((error) => { console.error(error); }).done((e) => { return true })
        } catch (err) {
            console.log(err)
        }
    }

}
